<?php

namespace App\Http\Controllers;

use App\Events\TodoCreatedEvent;
use App\Notifications\TodoCreated;
use App\Todo;
use http\Client\Curl\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class TodosController extends Controller
{
    public function index() {
        return view('welcome');
    }

    public function insert(Request $r){
        // INSERT DATA
        $todo = Todo::saveData($r);

        // CREATE EVENT
        event(new TodoCreatedEvent($todo));

        Session::flash('success', 'ok');

        return back();
    }
}
