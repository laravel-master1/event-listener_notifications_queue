<?php

namespace App\Http\Controllers;

use App\Todo;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class NotificationController extends Controller
{
    public function index(){
        $user = Auth::user();
        $notifications = $user->unreadNotifications;
        return view('notifications', compact('notifications'));
    }

    public function view(Request $r){
        // Mark as read
        DB::table('notifications')->where('id', $r->notification_id)->update([
            'read_at' => now()
        ]);

        $todo = Todo::where('id', $r->todo_id)->first();
        return view('notification_view', compact('todo'));
    }
}
