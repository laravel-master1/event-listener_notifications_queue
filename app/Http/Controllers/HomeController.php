<?php

namespace App\Http\Controllers;

use App\Todo;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $todos = Todo::orderBy('id', 'desc')->get();
        return view('home', compact('todos'));
    }
}
