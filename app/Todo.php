<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Todo extends Model
{
    protected $fillable = ['todo', 'desc', 'fk_user_id'];

    public static function saveData($r){
        $todo = Todo::create([
            'fk_user_id' => Auth::user()->id,
            'todo' => $r->todo,
            'desc' => $r->desc
        ]);

        return $todo;
    }
}
