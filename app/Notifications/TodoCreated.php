<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class TodoCreated extends Notification
{
    use Queueable;

    public $todo;

    public function __construct($todo)
    {
        $this->todo = $todo;
    }

    public function via($notifiable)
    {
        return ['database'];
    }


    public function toDatabase(){
        return [
            'title' => $this->todo->todo,
            'desc' => $this->todo->desc,
            'id' => $this->todo->id
        ];
    }

    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
