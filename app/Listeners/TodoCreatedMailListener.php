<?php

namespace App\Listeners;

use App\Jobs\SendTodoCreatedMailJob;

class TodoCreatedMailListener
{
    public function handle($event)
    {
        dispatch(new SendTodoCreatedMailJob($event->todo));
    }
}
