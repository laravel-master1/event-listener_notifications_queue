<?php

namespace App\Listeners;

use App\Notifications\TodoCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Auth;

class TodoCreatedNotificationListener
{
    public function handle($event)
    {
        $user = Auth::user();
        $user->notify(new TodoCreated($event->todo));
    }
}
