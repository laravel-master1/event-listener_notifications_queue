<?php

use Illuminate\Support\Facades\Route;

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


// My Routes
Route::get ('/',            'TodosController@index');
Route::post('/todo-insert', 'TodosController@insert')->name('todo.insert');


Route::get ('/notifications',       'NotificationController@index')->name('notifications');
Route::post('/notifications/view',  'NotificationController@view')->name('notifications.view');
