@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Notifications</div>
                    <div class="card-body">
                        @if(count($notifications) > 0)
                            @foreach($notifications as $n)
                                <div class="alert alert-info" style="width: 100%; cursor: pointer;" id="list-{{ $n->data['id'] }}" onclick="viewNotification({{ $n->data['id'] }}, '{{ $n->id }}')" >
                                    <p style="display: inline">New todo created title with <b>{{ $n->data['title'] }}</b></p>
                                </div>
                            @endforeach
                        @else
                            <p>No new notifications</p>
                        @endif

                        <a style="color: white;" class="btn btn-dark btn-sm btn-block">Show all notifications</a>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" id="modalBody">

            </div>
        </div>
    </div>



@endsection

@section('scripts')
    <script>
        function viewNotification(id, notification_id) {
            // change color
            var selected = '#list-'+id;
            $(selected).removeClass('alert-info');
            $(selected).addClass('alert-success');

            $.ajax({
                type: 'POST',
                url: "{!! route('notifications.view') !!}",
                cache: false,
                data: {
                    _token: "{{csrf_token()}}",
                    'todo_id': id,
                    'notification_id': notification_id
                },
                success: function (data) {
                    $('#modalBody').html(data);
                    $('#myModal').modal('show');
                }
            });
        }
    </script>
@endsection
