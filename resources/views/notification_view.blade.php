
<div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Notification</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>
<div class="modal-body">
    <p>Title : {{ $todo->todo }}</p>
    <p>Description : {{ $todo->desc }}</p>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
</div>
