@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Todos</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('todo.insert') }}">
                        @csrf
                        <fieldset>
                            <input class="form-control" type="text" name="todo" placeholder="Title" required>
                            <textarea class="form-control mt-3" name="desc" placeholder="Description" required></textarea>

                            <button type="submit" class="btn btn-primary mt-3">Save Todo</button>
                        </fieldset>
                    </form>

                    <div style="margin-top: 20px;">
                        <ul>
                            @foreach($todos as $todo)
                                <li>{{ $todo->todo }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
